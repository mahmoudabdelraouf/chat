<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/chat.css')}}">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <title>Chat</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Chat') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
        <div class="container clearfix">
            <div class="people-list" id="people-list">
              <ul class="list">
                @forelse($users as $user)  
                <li class="clearfix" onclick="changeUser({{$user->id}})">
                  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_01.jpg" alt="avatar" />
                    <div class="about" >
                    <div class="name" id="user{{$user->id}}">{{$user->name}}</div>
                  </div>
                </li>
                @empty
                <p class="center">No users founds</p>
                @endforelse
              </ul>
            </div>
            @if($users->count() > 0)            
            <div class="chat">
                <input type="hidden" id="reciverId" value="{{$users->first()->id}}">
                <div class="chat-header clearfix">
                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_01_green.jpg" alt="avatar" />
                
                <div class="chat-about">
                  <div class="chat-with" id="clientName">{{$users->first()->name}}</div>
                  <div class="chat-num-messages" onclick="clearMessage()">clear message</div>
                </div>
                <i class="fa fa-star"></i>
              </div> <!-- end chat-header -->
              
              <div class="chat-history">
                <ul id="MessageList">
                  @forelse($users->first()->messages() as $message)  
                  <li class="clearfix">
                    <div class="message-data {{$message->sender_id == auth()->user()->id ? 'align-right' : '' }}">
                      <span class="message-data-time" >10:10 AM, Today</span> &nbsp; &nbsp;
                      <span class="message-data-name" >{{$message->sender->name}}</span> <i class="fa fa-circle me"></i>
                      
                    </div>
                    <div class="message other-message {{$message->sender_id == auth()->user()->id ? 'float-right' : '' }}">
                      Hi Vincent, how are you? How is the project coming along?
                    </div>
                  </li>
                  @empty
                  @endforelse
                </ul>
                
              </div> <!-- end chat-history -->
              
              <div class="chat-message clearfix">
                <textarea name="message-to-send" id="message" placeholder ="Type your message" rows="3"></textarea>
                        
                <i class="fa fa-file-o"></i> &nbsp;&nbsp;&nbsp;
                <i class="fa fa-file-image-o"></i>
                
                <button id="submitMessage">Send</button>
        
              </div> <!-- end chat-message -->
              
            </div> <!-- end chat -->
            @endif
          </div> <!-- end container -->
        
        <script id="message-template" type="text/x-handlebars-template">
          <li class="clearfix">
            <div class="message-data align-right">
              <span class="message-data-time" >, Today</span> &nbsp; &nbsp;
              <span class="message-data-name" >Olia</span> <i class="fa fa-circle me"></i>
            </div>
            <div class="message other-message float-right">
              messageOutput
            </div>
          </li>
        </script>
        
        <script id="message-response-template" type="text/x-handlebars-template">
          <li>
            <div class="message-data">
              <span class="message-data-name"><i class="fa fa-circle online"></i> Vincent</span>
              <span class="message-data-time">, Today</span>
            </div>
            <div class="message my-message">
              response
            </div>
          </li>
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="{{asset('js/app.js')}}"></script>
        <script src="{{asset('js/chat.js')}}"></script>
        <script src="https://js.pusher.com/5.0/pusher.min.js"></script>

        <script>
            Pusher.logToConsole = true;
            var message="";
            var reciverId=0;
            $("#submitMessage").click(function(){
                message=$("#message").val();
                reciverId=$("#reciverId").val();
                messages(message,reciverId)
             });
            var pusher = new Pusher("22c9cca65efb5106147e", {
                cluster: 'eu',
                encrypted: true
            });
            var channel = pusher.subscribe('user_channel_{{auth()->user()->id}}');
            channel.bind('my-event', function(data) {
              console.log(data.receiver_id);
              $("#user"+data.receiver_id).css("color", "red");
              if($("#reciverId").val() == data.receiver_id)
              {
                var message=$("#message").val();
                var reciverId=$("#reciverId").val();
                messages(message,reciverId);
              }

          });
          function changeUser(id)
          {
            $("#reciverId").val(id)
            $("#clientName").html($("#user"+id).html());
            $("#user"+id).css("color", "white");
            var message=$("#message").val();
            var reciverId=$("#reciverId").val();
            messages(message,reciverId);

          }
          function clearMessage()
          {
            var reciverId=$("#reciverId").val();
            $.ajax({
                    url: "{{route('message.clear')}}",
                    data:{receiver_id:reciverId},
                    type: "Post", // not POST, laravel won't allow it
                    success: function(data){
                        $("#MessageList").empty().html(data);
                        $("#message").val('');
                    },
                    error: function(data){
                    },
                });
          }
          function messages(message,reciverId)
          {
            if($("#message").val().length < 150)
                {
                $.ajax({
                    url: "{{route('messages.store')}}",
                    data:{message:message,receiver_id:reciverId},
                    type: "Post", // not POST, laravel won't allow it
                    success: function(data){
                        $("#MessageList").empty().html(data);
                        $("#message").val('');
                    },
                    error: function(data){
                    },
                });
                }
                else
                {
                alert("message length must less than 150 char");
                }
           
          }
        </script>
    </body>
</html>
