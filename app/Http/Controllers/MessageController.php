<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\User;
use Pusher;
class MessageController extends Controller
{
    //
    public function index()
    {
        $messages1=Message::where('sender_id',1)->where('receiver_id',2)->get();
        $messages2=Message::where('sender_id',2)->orwhere('receiver_id',1)->get();
        $messages=$messages1->concat($messages2);
        return $messages;
        $messages=$messages->unique();
        
    }
    public function store(Request $request)
    {
        $request['sender_id']=auth()->user()->id;
        if($request->message !="")
        {
            Message::create($request->toArray());
            $data['message'] = 'hello world';
            $data['receiver_id'] = auth()->user()->id;
            $this->send_notification_users($request->receiver_id,$data);
        }
        $messages1=Message::where('sender_id',auth()->user()->id)->where('receiver_id',$request->receiver_id)->get();
        $messages2=Message::where('sender_id',$request->receiver_id)->where('receiver_id',auth()->user()->id)->get();
        $messages=$messages1->concat($messages2);
        $messages=$messages->unique();
        $messages=$messages->sortBy('id');
     
        return view('messages',compact('messages'));
    }
    public function clear(Request $request)
    {
        $request['sender_id']=auth()->user()->id;
        $messages1=Message::where('sender_id',auth()->user()->id)->where('receiver_id',$request->receiver_id)->delete();
        $messages2=Message::where('sender_id',$request->receiver_id)->where('receiver_id',auth()->user()->id)->delete();
        return "";
    }
    function send_pusher_notification($channel,$event,$data){
        $pusher = new Pusher\Pusher("22c9cca65efb5106147e", "98177fa2c7361501845e", "901349", array('cluster' => "eu") );
        $pusher->trigger($channel, $event, $data);

    }
    
    function send_notification_users($user,$data){
                $this->send_pusher_notification('user_channel_'.$user,'my-event',$data);
        }
    
}
